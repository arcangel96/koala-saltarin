/*
  Needs proper license disclaimer https://fullcalendar.io/license

 */

//Global Variables
var eventData; 
var currentEventId;

function showReservationDialog(reservationDate,start_time,end_time,desktop)
{
	var txtDate       = document.getElementById("txtDate");
	var txtDuration   = document.getElementById("txtDuration");
	var txtAvailable  = document.getElementById("txtAvailable");
	var txtDesktop    = document.getElementById("txtDesktop");
	var txtReservedBy = document.getElementById("txtReservedBy");

	txtDate.value = reservationDate;
	txtDuration.value =  start_time + " - " +  end_time;
	txtDesktop.value  = desktop;
	$( "#dialog-confirm" ).dialog( "open" ); // Shows the Reservation Dialog Box
}

function getTodaysDate() 
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	today = yyyy + '-' + mm + '-' + dd;

	return today;
}

/*
 * This function copies the start and end times
 * to the html textboxes.
 */
function getStartEndTime(start, end) 
{
	var a = new Date(start);
	var b = new Date(end);

	var times = new Array();

	times['start'] = a.toLocaleTimeString('en-US',{hour: '2-digit', minute:'2-digit'});
	times['end']   = b.toLocaleTimeString('en-US',{hour: '2-digit', minute:'2-digit'});
	return times;
}

function BuildCalendar() {

	$(document).ready(function() {

		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height: "auto",
			width: 400,
			autoOpen: false,
			modal: true,
			buttons: [
				{
					id: "btnRequest",
					text: "Request",
					icon: "ui-icon-plus",
					click: function() {
						$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
						$( this ).dialog( "close" );}
				},
				{
					id: "btnRelease",
					text: "Release",
					icon: "ui-icon-minus",
					click: function() {
						$('#calendar').fullCalendar('removeEvents',currentEventId);
						$( this ).dialog( "close" );}
				}
				]
		});



		$('#calendar').fullCalendar({

			schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			timezone: 'local',
			defaultView: 'agendaWeek',
			resourceGroupField: 'desktop',
			navLinks: true, // can click day/week names to navigate views
			selectable: true,
			selectHelper: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			allDaySlot:false,
			slotDuration: "03:00:00",
			contentHeight: 'auto',
			eventDurationEditable: false, //prevents event from being resize

			resources: [
				{ id: 'MES1', desktop: 'TI-12', title: 'MES1', eventColor: 'blue' },
				{ id: 'MES2', desktop: 'TI-16', title: 'MES2', eventColor: 'green' },
				{ id: 'MES3', desktop: 'TI-16', title: 'MES3', eventColor: 'orange' }
				],

				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,timelineDay'

				},

				defaultDate: getTodaysDate(),

				select: function(start, end) {

					var quantity = $('.fc-event').length;
					if (quantity > 3)
					{
						$('#calendar').fullCalendar('unselect');
						// alert('You can only select a maximum of three slots!')
						return
					}


					end = start + 1.08e+7; // enforces the 3hr blocks. (milliseconds)
					var times   = getStartEndTime(start, end);
					var desktop = document.getElementById("Desktop").value;
					var reservationDate = new Date(start).toLocaleDateString();
					var start_time = times['start'];
					var end_time   = times['end'];

					var releaseButton = document.getElementById("btnRelease");
					releaseButton.disabled = true;

					var requestButton = document.getElementById("btnRequest");
					requestButton.disabled = false;

					showReservationDialog(reservationDate,start_time,end_time,desktop);					

					var title =  quantity + "-" + document.getElementById("Build").value + "- Abreu";
					if (title) {

						eventData = {
								resourceId: document.getElementById("Desktop").value,
								title: title,
								start: start,
								end: end,
								desktop: desktop,
								reservationDate: reservationDate,
								start_time: start_time,
								end_time: end_time,
								user: "Abreu"
						};
						//$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

					}
					$('#calendar').fullCalendar('unselect');
				},

				eventClick: function(event, element) 
				{
					var requestButton = document.getElementById("btnRequest");
					requestButton.disabled = true;

					var releaseButton = document.getElementById("btnRelease");
					releaseButton.disabled = false;

					currentEventId = event._id;
					showReservationDialog(event.reservationDate,event.start_time,event.end_time,event.desktop);
					$('#calendar').fullCalendar('updateEvent', event);
				},

				eventOverlap: function(stillEvent, movingEvent) 
				{
					return stillEvent.allDay && movingEvent.allDay;
				},

				eventDrop: function(event, delta, revertFunc) {

					setStartEndTime(event.start, event.end);
				},

		        eventMouseover: function (data, event, view) {

		            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#ffffe6;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'Desktop ' + ': ' + data.desktop + '</br>' + 'User ' + ': ' + data.user + '</div>';


		            $("body").append(tooltip);
		            $(this).mouseover(function (e) {
		                $(this).css('z-index', 10000);
		                $('.tooltiptopicevent').fadeIn('500');
		                $('.tooltiptopicevent').fadeTo('10', 1.9);
		            }).mousemove(function (e) {
		                $('.tooltiptopicevent').css('top', e.pageY + 10);
		                $('.tooltiptopicevent').css('left', e.pageX + 20);
		            });


		        },
		        eventMouseout: function (data, event, view) {
		            $(this).css('z-index', 8);

		            $('.tooltiptopicevent').remove();

		        },



		});

	});
} // BuildCalendar